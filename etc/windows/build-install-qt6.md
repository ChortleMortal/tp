## Fetching

Use maintenance tool and download

> - source
> - CMake
> - Ninja


## Building for win10

From command Prompt:

     Ensure that the following tools can be found in the path:
     
     ActivePerl - Install a recent version of ActivePerl (download page).
     Python     - Install Python from the here.
    
     * Perl version 5.12 or later   [http://www.activestate.com/activeperl/]
     * Python version 2.7 or later  [http://www.activestate.com/activepython/]
     * Ruby version 1.9.3 or later  [http://rubyinstaller.org/]

Create directory 'static' alongside 'Source' directory

Run file qt6.bat  (copy from /etc directory)
```
@echo off
CALL "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"

SET _ROOT=C:\Qt\6.2.0\Src
SET PATH=%_ROOT%\qtbase\bin;%PATH%
SET _ROOT=
cmd /k
```
Some comments:
1.	-debug-and-release configures and builds OK, but the install does not install all the debug files
2.	Order is important:  do -debug first, then -release, otherwise does not work
3.	QtSvg is now nicely part of the the standard build.
```
configure -static -debug-and-release -no-pch -confirm-license -prefix "C:\Qt\6.2.0\static" -opensource -mp -nomake examples -nomake tests -no-icu -no-opengl -no-openssl -skip qtwayland -skip qtquick3d -skip qtquickcontrols2 -skip qtquicktimeline -skip qtshadertools -skip qtdeclarative -skip qttools -skip qtdoc -skip qttranslations -skip qt3d -skip qt5compat -skip qtcharts -skip qtcoap -skip qtdatavis3d -skip qtlottie -skip qtmqtt -skip qtopcua -skip qtscxml -skip qtvirtualkeyboard -skip qtmultimedia -skip qtwebengine -skip qtwebview
```
```
Run 'cmake --build . --parallel'

Once everything is built, you must run 'cmake --install .'

Qt will be installed into 'C:/Qt/6.2.0/static'

To configure and build other Qt modules, you can use the following convenience script in the Src directory:
        C:/Qt/6.2.0/static/bin/qt-configure-module.bat

If reconfiguration fails for some reason, try to remove 'CMakeCache.txt' from the build directory

```

- To create installer,  use NSIS compiler on:  C:\dev\tp2\etc\TiledPatternMaker.nsi


