## Fetching

```
$ git clone git://code.qt.io/qt/qt5.git
$ cd qt5
$ git checkout 5.12
$ git checkout v5.12.8
```

## Building for win10

From command Prompt:

     Ensure that the following tools can be found in the path:
     * Supported compiler (Visual Studio 2012 or later,
        MinGW-builds gcc 4.9 or later)
     * Perl version 5.12 or later   [http://www.activestate.com/activeperl/]
     * Python version 2.7 or later  [http://www.activestate.com/activepython/]
     * Ruby version 1.9.3 or later  [http://rubyinstaller.org/]


Create file qt5.bat in G:\qt5static
```
@echo off
REM Set up \Microsoft Visual Studio 2015, where <arch> is \c amd64, \c x86, etc.
REM CALL "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" <arch>
REM CALL "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat"
CALL "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"

REM Edit this location to point to the source code of Qt
SET _ROOT=C:\Qt\5.15.2\Src

SET PATH=%_ROOT%\qtbase\bin;%_ROOT%\gnuwin32\bin;C:\Qt\Tools\QtCreator\bin;%PATH%

REM Uncomment the below line when using a git checkout of the source repository
SET PATH=%_ROOT%\qtrepotools\bin;%PATH%

REM Uncomment the below line when building with OpenSSL enabled. If so, make sure the directory points
REM to the correct location (binaries for OpenSSL).
REM SET PATH=C:\OpenSSL-Win32\bin;%PATH%

REM When compiling with ICU, uncomment the lines below and change <icupath> appropriately:
REM SET INCLUDE=<icupath>\include;%INCLUDE%
REM SET LIB=<icupath>\lib;%LIB%
REM SET PATH=<icupath>\lib;%PATH%

REM Contrary to earlier recommendations, do NOT set QMAKESPEC.

SET _ROOT=

REM Keeps the command line open when this script is run.
cmd /k
```

```

```

In directory \qt5

```
.\configure -static -release -no-pch -confirm-license -prefix "C:\Qt\5.15.2\static" -opensource -mp -nomake examples -nomake tests -no-icu -no-opengl -no-angle -no-openssl -skip webengine
```

In qt5\qtbase:

```
nmake or jom
nmake install  or jom install
```

In \qt5\qtvsvg
```
nmake
make install
```


- To create installer,  use NSIS compiler on:  C:\dev\tp2\etc\TiledPatternMaker.nsi



