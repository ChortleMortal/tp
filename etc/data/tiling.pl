#!/usr/bin/perl
use strict;
use warnings;

use feature qw(say);

#my $file = $ARGV[0];
my $file = '/c/dev/tp/etc/data/tiling_data.txt';

open my $info, $file or die "Could not open $file: $!";

#chomp(my @lines = <$info>);
#print @lines;

my $bit1 = 'Info    : ========== "';
my $bit2 = '" ==========';
my $bit3 = '" ==========';
my $bit3a = '.xml';

my $bit4 = 'Info    : "Filled" algorithm:';
my $bit5 = 'cleanse:';

my @result = ();
$result[0] = '';
$result[1] = '';
my @data = ();
$data[0] = '';
$data[1] = '';

while( my $line = <$info>)
{
	if (index($line, 'Info') != -1) 
	{
		print $result[0],',',$result[1],',',$data[0],',',$data[1],"\n";
		$result[1] = '';
		$data[0] = '';
		$data[1] = '';
		
		$line =~ s/$bit1//; 
		$line =~ s/$bit2//; 
		$result[0] = $line;
		$result[0] =~ s/^\s+|\s+$//g;	#trim whitespace
		
	}
	elsif (index($line, 'Debug') != -1) 
	{
		$result[1] = substr($line,36);
		$result[1] =~ s/^\s+|\s+$//g;	#trim whitespace
		#$data[1] =~ s/^\s+|\s+$//g;	#trinm whitespace
	}
}


close $info;


