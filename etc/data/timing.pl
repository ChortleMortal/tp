#!/usr/bin/perl
use strict;
use warnings;

my $file = $ARGV[0];

open my $info, $file or die "Could not open $file: $!";

#chomp(my @lines = <$info>);
#print @lines;

my $bit1 = 'Info    : Load operation for ';
my $bit2 = 'took';
my $bit3 = 'seconds';

while( my $line = <$info>)
{
    $line =~ s/$bit1//; 
	$line =~ s/$bit3//; 
    #print $line;
	my @data = split $bit2, $line;
	#print @data;
	@data[0] =~ s/^\s+|\s+$//g;	#trim whitespace
    @data[1] =~ s/^\s+|\s+$//g;	#trinm whitespace

    print @data[0], ',' , @data[1], "\n";
}

close $info;

