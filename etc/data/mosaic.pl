#!/usr/bin/perl
use strict;

my $file = '/c/dev/tp/etc/data/mosaic_tiling.txt';

open my $info, $file or die "Could not open $file: $!";

#chomp(my @lines = <$info>);
#print @lines;

my $bit1 = 'Debug   : ========== "C:/dev/tp/media/designs/new_designs/';
my $bit2 = 'Debug   : ========== "C:/dev/tp/media/designs/original/';
my $bit3 = '" ==========';
my $bit3a = '.xml';

my $bit4 = 'Info    : tiling:';
my $bit5 = 'cleanse:';

my @result = ();

for (my $i = 0; $i < 8; $i = $i + 1)
{
	$result[$i] = '';
}

while( my $line = <$info>)
{
	if (index($line, 'Debug') != -1) 
	{
		if (index($line, 'Vertex Edge counts') == -1)
		{
			for (my $i = 0; $i < 8; $i = $i + 1)
			{
				print("$result[$i],");
				$result[$i] = '';
		    }
			print "\n";
			
			$line =~ s/$bit1//; 
			$line =~ s/$bit2//; 
			$line =~ s/$bit3//; 
			$line =~ s/$bit3a//; 
			$result[0] = $line;
			$result[0] =~ s/^\s+|\s+$//g;	#trim whitespace
		}
	}
	elsif (index($line, 'Info') != -1 && index($line, 'Filled') == -1 ) 
	{
		if (index($line,'tiling:') != -1)
		{
			$line =~ s/$bit4//; 
			$line =~ s/^\s+|\s+$//g;	#trim whitespace
			$result[1] = $line;
		}
		elsif (index($line,'has intersecting edges') != -1)
		{
			$result[2] = 'y';
		}
	}
	elsif (index($line, 'Warning') != -1)
	{
		$line = substr($line,28);
		$line =~ s/^\s+|\s+$//g;	#trim whitespace
		if ($line eq 'tiled overlaps')
		{
			$result[3] = 'y';
		}
		elsif ($line eq 'proto map verify errors')
		{
			$result[4] = 'y';
		}
		elsif ($line eq 'proto map intersecting edges')
		{
			$result[5] = 'y';
		}
		elsif ($line eq 'fill map verify errors')
		{
			$result[6] = 'y';
		}
		elsif ($line eq 'fill map intersecting edges')
		{
			$result[7] = 'y';
		}
		elsif ($line eq 'proto map verify errors')
		{
			$result[8] = 'y';
		}
	}		
}


close $info;


