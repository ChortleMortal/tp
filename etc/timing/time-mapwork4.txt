10 ,    3.958 
10.v1 ,    3.214 
12 ,    0.281 
12-6-4 ,    0.514 
12-6-4.v1 ,    0.503 
12-6-4.v2 ,    0.488 
12-6-4.v3 ,    0.492 
12-6-trigon ,    0.495 
12-6-trigon.v1 ,    0.494 
12-6-trigon.v2 ,    0.483 
12-6-trigon.v3 ,    0.486 
12-6-trigon.v4 ,    0.500 
12-6-trigon.v6 ,    0.421 
12.4.3 ,   13.029 
12.4.3.v1 ,   13.008 
12.v1 ,    0.314 
12.v2 ,    0.960 
12.v3 ,    0.960 
3-8-12 ,    0.165 
3-8-12.v1 ,    0.167 
3-8-12.v2 ,    2.389 
3-8-4-6 ,    0.578 
3-8-4-6.v1 ,    0.583 
3-8-4-6.v5 ,    1.108 
6-4-3 ,    0.357 
6-4-3-Pomerantz ,    0.087 
6-4-3.v1 ,    0.378 
6.5 ,    0.518 
6.5.v1 ,    0.529 
6.5.v2 ,    1.083 
6.5.v3 ,    0.560 
6.5.v4 ,    0.505 
7-shadow ,    0.759 
7-shadow.v1 ,    0.755 
7.4 ,    0.614 
7.4.v1 ,    0.614 
7.6 ,    0.752 
7.6.v1 ,    0.749 
8 6 almost 5 -- starred ,    1.712 
8 6 almost 5 -- starred.v1 ,    1.705 
8 and 8 rings carpet ,    3.848 
8 and 8 rings carpet.v1 ,    3.838 
8-6-5 ,   13.815 
8-6-5.v1 ,   13.776 
8.5 ,    0.366 
8.5.v1 ,    0.357 
8.6 ,    4.094 
8.6.6 ,    2.146 
8.6.6.v1 ,    2.318 
8.6.6.v2 ,    2.314 
9-6 Losange ,    1.313 
9-6 Losange.v1 ,    1.328 
9.6 almost 5 ,    1.769 
9.6 almost 5.v1 ,    1.764 
Abbasid Al-Mustansiriyya Madrasa ,    2.111 
Abbasid Al-Mustansiriyya Madrasa.v1 ,    2.103 
Add Doubt Sin ,    0.263 
Add Doubt Sin.v1 ,    0.263 
African Birds Migration ,    5.160 
African Birds Migration.v1 ,    5.141 
Al-Mustansiriyya Gold ,    2.097 
Al-Mustansiriyya Gold.v1 ,    2.102 
Al-Mustansiriyya Gold.v2 ,    2.012 
Alcazar of Seville ,    2.848 
Alcazar of Seville.v1 ,    2.850 
Alcazar of Seville.v2 ,    2.846 
Alhambra - Golden Room ,    0.254 
Alhambra - Golden Room.v1 ,    0.231 
Alien TO ,    0.082 
Alien TO.v1 ,    0.080 
Almost ,    2.614 
Almost.v1 ,    2.604 
Are you a square ,    0.262 
Are you a square.v1 ,    0.258 
Bird ,    0.077 
Bird.v1 ,    0.081 
Bird.v2 ,    0.088 
Bird.v3 ,    0.070 
Bird.v4 ,    1.160 
Bird.v5 ,    0.713 
Blood Vessels ,    3.350 
Blood Vessels.v1 ,    3.344 
Bloom of May ,   17.189 
Bloom of May.v1 ,   17.297 
Bluer shade of sky ,    0.307 
Bluer shade of sky.v1 ,    0.294 
Bone ,    0.330 
Bone.v1 ,    0.466 
Bone.v2 ,    0.161 
Broug Abd al-Samad Complex ,    0.676 
Broug Ben Yusuf Madrasa ,   10.185 
Broug Ben Yusuf Madrasa.v1 ,    4.675 
Broug Ben Yusuf Madrasa.v3 ,    7.954 
Broug Capella Palatina ,    0.158 
Broug Capella Palatina.v1 ,    0.174 
Broug East Tower Kharraqan ,    0.446 
Broug East Tower Kharraqan.v2 ,    0.410 
Broug Esrefoglu ,    0.091 
Broug Esrefoglu.v1 ,    0.059 
Broug Esrefoglu.v2 ,    0.093 
Broug Esrefoglu.v3 ,    0.082 
Broug Esrefoglu.v4 ,    0.162 
Broug Great Mosque Cordoba ,    0.661 
Broug Great Mosque Cordoba.v1 ,    0.725 
Broug Great Mosque Cordoba.v2 ,    0.731 
Broug Great Mosque of Damascas ,    0.160 
Broug Great Mosque of Herat ,    2.441 
Broug Great Mosque of Kairouan ,    1.948 
Broug Great Mosque of Kairouan.v1 ,    2.033 
Broug Huand Hatun Complex ,    0.802 
Broug Huand Hatun Complex.v1 ,    0.848 
Broug Huand Hatun Complex.v2 ,    0.839 
Broug Huand Hatun Complex.v3 ,    1.252 
Broug Huand Hatun Complex.v4 ,    1.242 
Broug Huand Hatun Complex.v5 ,    1.248 
Broug Koran of Rashid al-Din ,    0.152 
Broug Koran of Rashid al-Din.v1 ,    0.164 
Broug Mosque of al-Salih Tala'i ,    2.368 
Broug Mosque of al-Salih Tala'i.v1 ,    3.892 
Broug Mosque of al-Salih Tala'i.v3 ,    0.288 
Broug Mosque of al-Salih Tala'i.v4 ,    3.839 
Broug Mustansiriya Madrasa ,    0.226 
Broug Mustansiriya Madrasa.v1 ,    0.282 
Broug Tomb of Jalal al-Din Hussein ,    8.042 
Candyland ,    3.132 
Candyland.v1 ,    3.139 
Chikipoo ,    1.659 
Chikipoo.v1 ,    1.654 
Child sky of a thousand suns ,    7.818 
Child sky of a thousand suns.v1 ,    7.844 
Circulatory System ,    0.122 
Circulatory System.v1 ,    0.116 
Circus ,    1.932 
Circus.v1 ,    1.921 
Coffee Hankerchief ,    1.825 
Coffee Hankerchief.v1 ,    1.827 
Curvy Hex ,    0.948 
Curvy Hex.v1 ,    0.958 
dentelle ,    4.871 
dentelle.v1 ,    4.890 
Elegant Monster ,    0.293 
Elegant Monster.v1 ,    0.289 
Elegant Monster.v2 ,    0.287 
Enneagram ,    0.063 
epc_014 ,    0.464 
epc_014.v1 ,    0.454 
epc_014.v2 ,    0.443 
epc_014.v3 ,    3.200 
First Curve ,    0.069 
First Curve.v1 ,    0.067 
First Curve.v2 ,    0.071 
First Curve.v3 ,    0.075 
First Curve.v4 ,    0.083 
Floor ,    3.088 
Flying Vistas of Earth ,    0.285 
Flying Vistas of Earth.v1 ,    0.288 
Girih Crab ,    4.515 
Girih Crab.v1 ,    4.534 
GirihInflation10 ,   13.265 
GirihInflation10.v1 ,   13.190 
GirihInflation10.v2 ,   14.162 
GirihInflation10.v3 ,   14.683 
GirihPat1 ,    3.404 
GirihPat1.v1 ,    3.415 
GirihPat1.v2 ,    5.621 
GirihPat1.v3 ,    2.129 
GirihPat1.v6 ,    7.371 
Golden Cage ,    1.633 
Golden Cage.v1 ,    1.636 
Green Islands ,    2.766 
Green Islands.v1 ,    2.776 
Green Islands.v2 ,    2.754 
Green Mosque ,    0.983 
Green Mosque.v1 ,    0.988 
Ilkhanid Uljaytu ,    1.302 
Ilkhanid Uljaytu.v1 ,    1.307 
Kumiko ,    0.639 
Kumiko.v1 ,    2.401 
leaf  ,    1.119 
Locked Ribbon ,    0.142 
Locked Ribbon.v1 ,    0.134 
Malatya ,    3.874 
Malatya.v1 ,    3.908 
Mamluk Quran ,    4.456 
Mamluk Quran.v1 ,    4.450 
Mexican Suns ,    3.103 
Mexican Suns.v1 ,    3.109 
Mexuar1 ,    0.433 
Mexuar1.v1 ,    0.447 
Mexuar1.v2 ,    0.437 
Mexuar1.v3 ,    0.440 
More alien ,    0.469 
More alien.v1 ,    0.439 
Museum1 ,    1.917 
Museum2 ,    1.787 
Museum2.v1 ,    1.744 
Museum2.v2 ,    1.741 
Museum2.v3 ,    1.742 
Museum2.v4 ,    1.748 
Nonagon ,    0.066 
Nonagon.v1 ,    3.951 
Nonagon.v2 ,    7.525 
Nonagon.v3 ,    9.337 
Nonagon.v4 ,    9.395 
Ochre Silicate ,    0.417 
Ochre Silicate.v1 ,    0.415 
pbn_7 ,    6.304 
pbn_7.v1 ,    6.277 
Penrose Dots ,    0.770 
Penrose Dots.v1 ,    0.758 
Penrose Monster ,    0.282 
Penrose Monster.v1 ,    0.285 
Petal ,    0.075 
pipes ,    2.850 
pipes.v1 ,    2.846 
PropagationTest ,    0.948 
PropagationTest.v1 ,    0.529 
Rhombus ,    1.623 
rhombus-10 ,    1.975 
Rhombus.v1 ,    3.629 
Rock the Hive ,    0.317 
Rock the Hive.v1 ,    0.317 
SarahsPlate ,    0.315 
SarahsPlate.v3 ,    0.297 
SarahsPlate.v4 ,    0.295 
SarahsPlate.v5 ,    0.295 
SimpleStar ,    0.746 
SimpleStar.v1 ,    0.747 
SimpleStar.v2 ,    0.756 
SPlateTest1 ,    0.272 
Squoc ,    0.544 
Squoc.v1 ,    0.559 
Squoctogon ,    0.274 
Squoctogon.v1 ,    0.271 
star ,    0.452 
star.v1 ,    0.443 
Sultan Lodge ,    2.093 
Sultan Lodge.v1 ,    2.068 
Tangle Diamonds ,    2.939 
Tangle Diamonds.v1 ,    2.951 
Test Pattern 2 ,    0.059 
Test Pattern 2.v1 ,    0.059 
Test Pattern 2.v2 ,    0.065 
Test Pattern 2.v3 ,    0.062 
Test00 ,    0.702 
Test01 ,    0.123 
Test02 ,    0.443 
Test02.v1 ,    0.451 
Test03 ,    0.538 
Test03.v1 ,    0.365 
Test03.v2 ,    0.931 
Test03.v3 ,    1.627 
Test04 ,    0.428 
Test04.v1 ,    0.713 
Test04.v2 ,    1.014 
Test05 ,    2.396 
Test05.v1 ,    2.523 
Test06 ,    5.868 
Test07 ,    1.544 
Test07.v1 ,    0.804 
Test08 ,    2.552 
Test08.v2 ,    2.565 
Test08.v5 ,    4.906 
Test08.v6 ,    2.625 
Test09 ,    1.083 
Test10 ,    5.193 
Test10.v1 ,   11.057 
Test10.v10 ,   20.519 
Test10.v2 ,   13.410 
Test10.v3 ,   13.481 
Test10.v8 ,   29.127 
Test10.v9 ,    5.126 
Test11 ,    4.282 
Test11.v1 ,    4.254 
Test11.v10 ,    2.159 
Test11.v11 ,    1.231 
Test11.v4 ,    4.217 
Test11.v6 ,    2.138 
Test11.v8 ,    2.140 
Test11.v9 ,    2.127 
Test12Rect ,    0.070 
Test12Rect.v1 ,    0.064 
Test12Rect.v2 ,    0.070 
Test12Rect.v3 ,    0.065 
Test12Rect.v4 ,    0.065 
Test12Rect.v5 ,    0.065 
Test12RectInterlace ,    0.060 
Test12RectOutline ,    0.054 
Test13 ,    3.148 
Test13.v1 ,    3.226 
Test13.v3 ,    1.558 
Test13.v4 ,    2.390 
Test13.v5 ,    3.599 
Test13.v6 ,    3.671 
Test13.v7 ,    1.464 
Test14 ,    1.046 
Test15 ,    1.125 
Test15.v1 ,    1.097 
Test15.v3 ,    1.087 
Test16 ,    0.637 
Test16.v1 ,    0.146 
Test16.v2 ,    0.146 
Test17 ,    0.058 
Test17.v1 ,    0.052 
Test17.v2 ,    0.049 
Test18 ,    0.062 
thickness ,    3.716 
thickness.v1 ,    3.739 
Timurid Tumaq Aqa ,    0.975 
Timurid Tumaq Aqa.v1 ,    0.978 
Timurid Tumaq Aqa.v2 ,    1.107 
Triangulated ,    1.695 
Triangulated.v1 ,    1.698 
uniform-12-3 ,    3.187 
USA ,    2.045 
USA.v1 ,    2.044 
UsingMapEdLoad1 ,    0.125 
Vanda ,    1.366 
Vanda.v1 ,    1.564 
Vanda.v10 ,    0.706 
Vanda.v2 ,    0.765 
Vanda.v3 ,    0.349 
Vanda.v4 ,    0.575 
Vanda.v5 ,    0.607 
Vanda.v6 ,    0.629 
Vanda.v7 ,    2.156 
Vanda.v8 ,    2.174 
Vanda.v9 ,    2.441 
Vanda2 ,    0.386 
Vanda2.v1 ,    0.366 
Vanda2.v2 ,    0.528 
Vanda2.v3 ,    0.840 
Vanda2.v4 ,    0.848 
Water Rings ,    1.712 
Water Rings.v1 ,    1.716 
wawa ,    1.070 
wawa.v1 ,    1.063 
wawa.v2 ,    1.069 
white stars ,    0.282 
white stars.v1 ,    0.283 
wip ,    0.273 
wip.v1 ,    0.270 
wonka ,    0.314 
wonka.v1 ,    0.301 
wonka.v2 ,    0.308 
wonka.v3 ,    0.301 
