### The Structure

- Think Controller - Model - View

- Tiling Maker deals with  Features and Tilings.   If there are prototypes or styles, they can be rebuilt as needed

- Figure Maker deals with Prototypes

- Style Maker deals with Styled Designs.

  

### About faces

The assumption is that each edge is only in two faces.  But this is not true if the edges overlap. If they do, the larger face should win (I think)

### Directions:

+ Faces are the tiles. They can have edge properties. If I was to make physical patterns I Would need faces. They should be equally as important as tiles and styles. More work here.
+ https://www.samiramian.uk/
+ You tube: Eric Broug
+ You tube: Met museum New York
+ Peterlu.org http://www.peterlu.org//content/decagonal-and-quasicrystalline-tilings-medieval-islamic-architecture
+ Penrose tiling
+ Kepler tilings. Harmony of the world in google
+ David Wade http://geometricism.com/

1. Viewers are actually QGraphicsItems but with their own draw methods (for the most part)
3. Confusion between a viewer and what is viewed
4. **many sig-ws calls should just be canvas invalidate calls**

+ ** Exporting from shape to map, so it can be styled and/or edited**
+ Consolidating the designs/styles/tiles already there
+ Looking at Broug again
+ Looking at borders

So this application is not Optimized for speed of execution.  It is inefficient, but the
focus is on the creating and drawing the designs, not on how long it takes to do that.
So this differs from Taprats which was more efficient.

Storing shapes as edges (lines & curves) rather than points is more inefficient too, since
the each point is stored twice.  However it does clarify whether a polygon is closed or not.

Right now, the map is built on demand, triggered by viewer generally
The on-demand model is faster but limited.
tiling_changed - rebuild prototypes and styles
figure_changed - rebuild prototypes and styles
style changed  - rebuild styles

when repeat mode changes, call createProtoMap(), then build styles too();  sig-render();


### Todo:
+ More than one tiling in a design - the means styles with different prototypes
+ Add stars/rosettes on points not mid-sides
+ Draw measurement lines
+ **Save/load Extended Figure parms**
+ Debug Extended figure when less than 2.0
+ Style designs etc. don't use activeDesigns.  So use of active designs needs to be examined in menus etc
+ Bug: while prototype is constructing changing mode (say from fill to defined) causes crash because map is erased underneath the constructor. Probably needs a semaphore to fix.  Low priority.

### Girih
The five shapes of the tiles are:

a regular decagon with ten interior angles of 144°;
an elongated (irregular convex) hexagon with interior angles of 72°, 144°, 144°, 72°, 144°, 144°;
a bow tie (non-convex hexagon) with interior angles of 72°, 72°, 216°, 72°, 72°, 216°;
rhombus with interior angles of 72°, 108°, 72°, 108°; and
a regular pentagon with five interior angles of 108°.

### Graphics

| Polygon | Line | Painter |
| ------- | ---- | ------- |
| Emboss| Emboss||
| Outline| Outline ||
| Filled ||
| Interlace | Interlace |
| Tiling DesignerView | TilingDesingerView | TilingDesignerView |
| DesignElementView | DesignElementView | DesignEelementView |
| FacesetView ||FaceSetView |
||| FigureView|
|| TilingView | TilingView |
||| MapEditorView |
||| PrototypeView |
|| TilingDesigner |
|| TilingMouseActions |
|| EdgePoly | EdgePoly |
||| Layer.h|
|||Misc|
|||Scene|
|||MapEditor|
|||MapMouseActions|
|||Style|
|||BackgroundImage|
||ItemViewer|
|||ShapeViewer|

### Test Procedure
+ Test all viewers and menus
+ start from style (top down)
	  - modify style
	  - modify figure
	  - modify tiling
+ start from tiling (bottom up)
   	+ modify tiling
      	+ modify figure
         	+ modify style
+ start from tiling and build up
+ start from nothing and build up
+ modify tiling and save
+ modify style and save

### design pattern
    QPolygonF  poly;
    QString astring;
    QDebug  deb(&astring);
    deb << poly;
    return astring;

### last modified time for each file
git ls-files -z | xargs -0 -n1 -I{} -- git log -1 --format="%ai {}" {} | sort

or

git ls-tree -r --name-only HEAD | while read filename; do echo "$(git log -1 --format="%ad" -- $filename) $filename "; done


### Done:

1. Tilings and Designs are loaded/saved from XML
2. Make tiling into a style by having figure points equal tiling points
3. Overlapped tilings
4. In tiling editor constrain movements to vertical or horizontal
5. Tiling Designer needs a refresh button or have actions cause update (especially save)
6. Save XML a) should say OK and b) should signal to loader to refresh
7. express rotation in degrees (not radians).
8. The trouble with the bridge is can't change style because pattern keeps being rebuilt.
9. Pattern parms for proto are hacks since they are really in the design not the pattern.
10. Viewers are bogus AND viewers keep incrementing - something wrong here  - look at design element page
11. Bug - not clearing between views
12. ** Building design and VIEWERS (showing design) should be two different things ***
13. Bug - proto alone wrong scale. switching between protos single/pack/multi - very weird
14. Figure view not centered (location is wrong)
15. Optimize workspace viewer code
16. Should there be wsStyles in addition to loadedStyles
17. If XML styles does not have .xml in text will sort properly
18. load/save of extended style is only working due to defaults
19. Cycler not going through all the designs - probably just one directory (should use the menu pages for the current list)
20. Figure editor not properly centering on LHS (maybe right too)
21. Make build and release executables different
22. Fix for double click on pack having different result than single
23. Background color stuff - needs a menu page
24. Get rid of cursor over panel page disabling updates
25. New Tile background color style
26. New Filled style algorithms
27. Loaders should actually load.  Tilings are pre loaded, designs are instantiated, and styles code prevents re-load of same code
28. Bug - Rosette and Star Maker not working
29. Not all viewers have transformers!! Need to keep track of transformers.
30. Sorting photos and starting to reproduce them again - that's where this started and that's where it was good.
31. Put back the figure and rosette shaped designs, and tile them.
32. Remove transformer and transformable classed - it's all in the layer
33. Removed Viewer class, its' in the workspace viewer (now renamed ViewerBase to Viewer)
33. Adds Map Editor
34. Deploy source code for release on gitlab
35. Add shapes to tiling which can be copied and rotated
36. Tiling should be grphicsitemgroup which has bounding rectangle
37. Revisit tiling editor
38. Borders fixed.  They remain in canvas sittings
40.  but now automatically adjust size
41. Need to decouple prototype from tiling maker.  Scope of tiling maker is just the tiling until push/Propagate
42. Got to do translates from centers
