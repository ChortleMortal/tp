## Map Editor:

#### Lines:
magenta feature
orange  radial figure boundary lines
yellow  extended boundary
green   figure map edges
white   construction

#### Points:
vertex blue
other yellow

## Figure Maker
green   figure map
red     radial figure boundary
magenta feature boundary
yellow  extended boundary
white   debug map

F - toggles radial replicate
K - toggles debug map

## Tiling editor:

#### Modes:
| Key  | Action | Function | Status |
| ---  | ------ | -------- | ------ |
|   F2 | Transform |                            | OK |
|   F3 | Translation Vectors | class DrawTranslation |Fixed|
|   F4 | Create Polygon | class CreatePolygon | Fixed |
|   F5 | Copy Polygon | class CopyMovePolygon |Fixed|
|   F6 | Delete Polygon | deletePolygon() |OK|
|   F7 | Include/Exclude Poly | toggleInclusion() | OK |
|   F8 | Show Position | Position | OK|
|   F9 | Measure | Measure | OK|
|   F10| Bkgd Perspective | Perspective |OK|
|   F11| Edit Feature | EditFeature |TODO|


#### Buttons:
| Key | Button | Action | Status |
| --- | ------ | -------- | ------ |
| A | Add Regular | | OK |
| C | Copy Sel |
| D | Delete Sel |
| E | Exclude All | |  OK |
| F | Fill using Vectors | | OK |
| I | Include Sel |
| R | Remove Excluded | | OK |
| V | Clear Vectors ||OK|
|   | Export Poly ||OK|
|   | Import Poly ||OK|

#### Mouse Clicks:
| Click | Mode   | Was | Change | Status |
| ----- | ------ | -------- | ------ | ------ |
| LEFT  | VERTEX | Create Polygon | Join Line ||
|       | MID_POINT | Join Edge |Join Line||
|       | EDGE | Join Edge | Join Edge | OK |
|       | INTERIOR | Move Polygon | Move Polygon | OK |
| MIDDLE |         | Draw Translation |||
| RIGHT  | VERTEX | Copy Move Polygon |Copy Join Line||
|        | MID_POINT | Copy Move Polygon |Copy Join Line||
|        | EDGE     | Copy Join Edge |Copy Join Edge||
|        | INTERIOR | Copy Move Polygon |Copy Move Polygon|OK|

####Snap Design:
- line to line (if lengths are the same)
- mid point to mid point
- p1 to p1
- p2 to p2


ideas
1. snap to grid
2. scale to grid somehow xform or bounds deltas
4. feature editor rounding
5. snap to point again was abandoned)
6. in table number of included
7. auto center tiling in view
