## ﻿Changes to taprats:

1. Added xstream 1.4.9 and wrote out designs as XML
2. Added flag to FillRegion to only show a single tile

## Achievements of taprats:

1. Powerful drawing engine
2. Generatiom of stars and rosettes very nice
3. Interlace styling very nice too.

## Questions about design:

1. Why only an n-pointed star/rosette in n-sided polygon/figure.  Why can't we put a 32 point star in a 4-sided square?
2. Why don't elements of tilings overlap.  I can see this might cause problemns the way interlace and filled styles are generated,
   but for thick or plain, overlapping seems to give useful results.
3. Triangles, pentagons, hexagons, etc have only one orienation.  Is it possible to rotate them to make tilings? I have added my own code
   to chnge the roatation (and the scale) of the featues by modifying the transform.  This is inded already done in some of the
   checked in example.  But I don't see how they got there unless the numbers were just types into vode or tiling file.
4. Points of stars and rosettes are set to be in the centres of polygon sides.  Is there a way to align them with the vertices
   of the polygon.
4. It seems that with explicit figures these are saved without the parameters that made them. The Styles can be modified but the figures
   placed into the the features are just edges and vertices in a map structure. Am I missing something?  So really existing designs can be
   decomposed or edited if they contain explicit figures.

1. 

## General Comments

1. Although taprats is impressive in what it can do, the examples look decidedly unlike the exemplars we have from Andalucia and Islamic society.  I think the emphasis has been on the tiling and this is based
- map/line based - no curves
