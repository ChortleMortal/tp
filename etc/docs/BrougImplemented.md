| Origin | Ref  | Name                        | Technique                            | Styled | Shaped | Status |
| ------ | ---- | --------------------------- | ----------------------------------   | ------ | ------ | ------ |
| Broug  | p27  | Great Mosque of Cordoba     |                                      |        |   X ||
| Broug  | p30  | Great Mosque of Kairouan    |                                      | X      |||
| Broug  | p34  | Mustansiriya Madrasa        | Hexagon Packing                      |        |   X ||
| Broug  | p37  | Esrefoglu Mosque            | a) Hexagon Packing                   | ?      |||
|        |      |                             | b) Hexagon inside hexagon (tiling)   |        |||
|        |      |                             |  + Extended Star (Figure)            | ?      |||
|        |      |                             | c) Resized Extended Star             | X      |||
| Broug  | p41  | Capella Palatina            | Four pointed rosette                 | X      | X       |colors not right in v1|
| Broug  | p43  | Koran of Rashid al-Din      |                                      | X      | X ||
| Broug  | p46  | Abd al-Samad Complex        | Placed Rectangles                    | X      |         ||
| Broug  | p49  | Great Mosque of Damascus    | Hexagon (tiling) + Star (Figure)     | X      |         ||
| Broug  | p52  | Great Mosque of Herat       |                                      | X      |         |Todo   |
| Broug  | p55  | Alhambra                    | Needs Curves                         |        | X       |Two versions |
| Broug  | p58  | East Tower of Kharraqan     | Using map editor according to Broug  | X      |         |Todo   |
| Broug  | p62  | Huand Hatun Complex         | Using map editor a la Broug          |        |||
| Broug  | p66  | Mosque of al-Salih Tala'i   |                                      |        |||
| Broug  | p71  | Ben Yusuf Madrasa           |                                      |        |||
| Broug  | p76  | Tomb of Jala al-din Hussein |                                      |        |||
| Broug  | p82  | Mosque of al-Nasir Muhammad |                                      |        |||
| Broug  | p88  | Mamluk Koran                |                                      |        |||
| Broug  | p99  | Tomb of Bibi Jawandi        |                                      |        |||
| Broug  | p104 | Qarawiyyin Mosque           |                                      |        |||
|        |      | Kumiko                      |                                      | X      | X ||
