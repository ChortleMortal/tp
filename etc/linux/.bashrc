alias tp='cd /c/dev/tp'
alias tp2='cd /c/dev/tp2'
alias tp3='cd /c/dev/tp3'
alias tp4='cd /c/dev/tp4'
alias gl="git log --pretty=oneline --abbrev-commit"
alias gs="git status"
alias gc="git checkout"
alias gp="git pull"
alias exp="explorer ."

