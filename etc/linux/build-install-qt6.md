## Fetching

Use maintenance tool and download

> - source
> - CMake
> - Ninja


## Building for Linux/X11

Pre-requisites:

sudo apt-get install build-essential &&
sudo apt install libglx-dev &&
sudo apt install libgl1-mesa-dev

sudo apt install    libfontconfig1-dev &&
sudo apt install    libfreetype6-dev &&
sudo apt install    libx11-dev &&
sudo apt install    libx11-xcb-dev &&
sudo apt install    libxext-dev &&
sudo apt install    libxfixes-dev &&
sudo apt install    libxi-dev &&
sudo apt install    libxrender-dev &&
sudo apt install    libxcb1-dev &&
sudo apt install    libxcb-cursor-dev &&
sudo apt install    libxcb-glx0-dev &&
sudo apt install    libxcb-keysyms1-dev &&
sudo apt install    libxcb-image0-dev &&
sudo apt install    libxcb-shm0-dev &&
sudo apt install    libxcb-icccm4-dev &&
sudo apt install    libxcb-sync-dev &&
sudo apt install    libxcb-xfixes0-dev &&
sudo apt install    libxcb-shape0-dev &&
sudo apt install    libxcb-randr0-dev &&
sudo apt install    libxcb-render-util0-dev &&
sudo apt install    libxcb-util-dev &&
sudo apt install    libxcb-xinerama0-dev &&
sudo apt install    libxcb-xkb-dev &&
sudo apt install    libxkbcommon-dev &&
sudo apt install    libxkbcommon-x11-dev &&

