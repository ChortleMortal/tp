## Fetching

```
$ git clone git://code.qt.io/qt/qt5.git
$ cd qt5
$ git checkout 5.12
$ git checkout v5.12.8
```


## Building for Linux/X11


```
 ./configure -static -release -confirm-license -prefix /home/david/QtStatic -opensource -mp -nomake examples -nomake tests -no-icu -no-opengl -no-angle -no-openssl -skip webengine

 nmake

 make install
```

Next build the application.  In /home/david/dev/tp2/src :

```

PATH=/home/david/QtStatic/bin:$PATH

export PATH

qmake -config release

make -j 4
```
Possibly also
```
QMAKESPEC=/home/david/QtStatic/mkspecs/linux-g++

export QMAKESPEC

```
